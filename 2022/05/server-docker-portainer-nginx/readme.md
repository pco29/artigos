# 0. Requisitos
 - maquina linux de preferencia ubuntu ou debian;
 - necessario no minimo 2gb de memoria ram. Lembro de ter tentado com 1 gb, mas ocorreu erros misteriosos no portainer;

# 1. Instalar o Docker

```console
user@host:~$ apt-get install docker docker.io docker-compose   # instala o docker
user@host:~$ useradd -Gdocker user                             # adiciona o usuario no grupo do docker
user@host:~$ id                                                # lista os grupos de um usuario  
```

* Fazer logout and logar novamente para atualizar os grupos dos usuarios, caso nao apareceu

# 2. Instalar o Portainer

Documentação Base:
- https://docs.portainer.io/v/ce-2.11/start/install/server/docker/linux
- https://hub.docker.com/r/portainer/portainer-ce

```
docker pull portainer/portainer-ce
docker volume create portainer_data
docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:2.11.1
```

Acessar no browser https://localhost:9443
Aceitar que nao existe o certificado


# 3. Instalar o Nginx-Proxy

Documentação Base:
- https://nginxproxymanager.com/guide/#project-goal

Copiar esse codigo para o portainer

```
version: '3'
services:
  app:
    image: 'jc21/nginx-proxy-manager:latest'
    restart: unless-stopped
    ports:
      - '80:80'
      - '81:81'
      - '443:443'
    volumes:
      - ./data:/data
      - ./letsencrypt:/etc/letsencrypt
```

Executar o comando


![tela de configuração](nginx-boot.png)


# Material de Apoio
https://stackoverflow.com/questions/45023363/what-is-docker-io-in-relation-to-docker-ce-and-docker-ee-now-called-mirantis-k
https://docs.docker.com/engine/install/ubuntu/
