Soberania
Serpro: A “Petrobras” das empresas de TI.
Bolsonaro ameaça privatizar o braço direito do Estado nacional.


O Brasil possui uma das maiores empresas públicas de TI do mundo, o Serpro. Esta empresa chegou a ter mais de 11 mil empregados no final dos anos 2010, com 10 regionais, 31 escritórios, 2 centros de dados em instalações subterrâneas, protegidas por murros de concreto resistentes a ataque aéreos, sala cofre, também é gestor de uma das maiores infraestrutura de redes públicas do mundo espalhados em todo o país.

O Serviço Federal de Processamento de Dados, Serpro, foi criado em 1964 para atender a Secretaria da Receita Federal, rapidamente passou a ser a empresa de TI de Ministério da Fazenda e hoje atende a boa parte do Governo Federal, presta serviços para empresas e para o cidadão diretamente.

Esta empresa desenvolveu soluções únicas e inigualáveis como a Conta Única do Tesouro Nacional e o SIAFI, que desde a década de 1980 dão conta com máxima eficiência, precisão e segurança das finanças de um país continental, tornando-se um modelo em todo o mundo. No inicio deste século o Serpro foi o principal fomentador da política de software livre e dados abertos do Governo Federal, corresponsável pela elaboração da ePing e do projeto software público.  O Serpro desenvolveu o Portal da Transparência, o GovBr, o Siscomex, o Importo de Renda, só para citar alguns. Diz-se que o Serpro é o principal responsável pela governança do Estado, seria como um “cockpit”, um painel de comandos pelo qual se administra o país.

O Presidente do Serpro escreveu no Relatório Integrado Anual de 2021: “Neste ano, tivemos o privilégio de apoiar o Ministério da Economia para posicionar o Brasil no ranking da ONU que avalia os 20 países que oferecem melhores serviços públicos online do mundo. Somos o 1º país da América do Sul e o 2º das Américas. Também comemoramos a posição brasileira na avaliação do Banco Mundial, que nos indicou como o 7º país do mundo com a maior maturidade em governo digital, o único com mais de 100 milhões de habitantes.”

A capacidade de inovação do Serpro é inesgotável e hoje ele produz soluções em áreas como big data, inteligência artificial, processamento de linguagem natural, visão computacional dentre outras.

A importância de ter uma empresa pública com esta capacidade é enorme, ela guarda dados vitais para o país e controla sistemas que dizem respeito diretamente a soberania nacional. Imagine  quão danoso seria para o país ter o sistema que gere a contabilidade pública invadido ou destruído? Imagine se de repente não fosse possível saber quanto dinheiro o Governo possui, onde foi gasto, se foi pago ou se está empenhado? Imagine se perdêssemos a capacidade de cobrar impostos ou de controlar a balança comercial?

Em um mundo cada vez mais conectado e dependente da tecnologia, onde ataques cibernéticos ocorrem diariamente, é um prodígio que estes sistemas mencionados anteriormente estejam praticamente imunes. Se por um lado não se houve falar de tentativas de invasão do território nacional, é constante as tentativas de invasão de sistemas públicos no Brasil. O Serpro é, nesse sentido, um dos, senão o maior, garantidor da soberania nacional.

Mas porque esta empresa é desconhecida da maioria dos brasileiros? A resposta não é simples, por um lado o Serpro não é uma empresa comercial, pelo menos não deveria ser, assim não se faz propaganda da empresa, mas de seus produtos. Outro aspecto é que o sucesso de muitos produtos do Serpro são atribuídos ao contratante, como por exemplo o SIAFI da Secretaria do Tesouro Nacional, o Siscomex do Ministério do Comercio Exterior, ou ainda o Imposto de Renda da Secretaria da Receita Federal. Uma empresa de TI aparece mais nas falhas que nos acertos e pra nossa sorte o Serpro acerta muito mais do que erra.

Mesmo tendo um faturamento relativamente alto, sendo R$ 390,9 milhões, o lucro no último ano, o Serpro tem mais valor e importância por questões estratégicas do que pelo lucro que produz, afinal qualquer empresa pode prestar os serviços que o Serpro presta, contudo colocar a soberania nacional nas mãos de empresas privadas ou pior ainda, estrangeiras, é algo impensável em qualquer lugar do mundo.

O Conselho de Administração do Serpro disse no mesmo Relatório Integrado Anual de 2021: “A segurança da informação está no seu DNA. A Empresa ampliou ainda mais seus investimentos, e por isso, a expertise do Serpro foi novamente reconhecida pelos seus clientes por meio do apoio constante e no sucesso das operações cotidianas e extraordinárias quando da prestação dos serviços. Para além disso, novos serviços da empresa nessa área de atuação reforçam essa posição de destaque na segurança da informação.” Mas logo em seguida se contradiz: “Incluído no Programa Nacional de Desestatização – PND desde 2020, o Serpro segue realizando sua finalidade, garantindo a disponibilidade e segurança de todos os seus serviços, alcançando resultados positivos para suas partes interessadas.”

As pretensões entreguistas do Governo Bolsonaro colocaram o Serpro na lista das empresas a serem privatizadas, um crime contra o país, equivalente a privatizar as forças armadas. Um golpe do qual o país pode levar décadas para se recuperar. O povo brasileiro precisa se erguer e defender o patrimônio que construiu ao longo de décadas ao custo de muito sofrimento. É hora dos partidos de esquerda, das organizações sociais e das lideranças sindicais despertarem e agirem para mobilizar as massas em defesa do Brasil, de seu patrimônio e de sua soberania.


